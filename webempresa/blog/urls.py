from django.urls import path
from . import views

urlpatterns = [
	#Paths de blog
	path('', views.blog, name="blog"),
	path('category/<int:category_id>/', views.category, name="category"),  #category/<int:category_id>/ me maneja dinamicamente el id de la categoria, como llega como cadena, se convierte en int
]
