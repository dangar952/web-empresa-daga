from django.shortcuts import render, redirect
from django.urls import reverse
from django.core.mail import EmailMessage
from .forms import ContactForm

# Create your views here.
def contact(request):
    contact_form = ContactForm() #Se crea una plantilla vacía

    if request.method == 'POST': #Validamos si se ha enviado información desde el formulario
        contact_form = ContactForm(data=request.POST) #Cargamos la información enviada desde el formulario
        if contact_form.is_valid():
            name = request.POST.get('name', '')
            email = request.POST.get('email', '')
            content = request.POST.get('content', '')
            #Enviamos el correo y redireccionamos
            email = EmailMessage(
                "La Caffettiera: Nuevo mensaje de contacto",
                "De {} <{}>\n\nEscribió:\n\n{}".format(name, email, content),
                "no-contestar@inbox.mailtrap.io",
                ["davidangarita@gmail.com"],
                reply_to=[email]
            )
            try:
                email.send()
                #Todo ha ido bien, Redireccionamos a Ok
                return redirect(reverse('contact')+"?ok") #le pedimos a django que cargue nuevamente la página contact y concatenamos un ok
            except:
                #Algo no ha ido bien, Redireccionamos a FAIL
                return render(reverse('contact')+"?fail")

    return render(request, "contact/contact.html", {'form': contact_form})